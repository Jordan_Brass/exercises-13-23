﻿using System;

namespace exercises_13_23
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
           
            // Exercise 13
            string myName = "Jordan";
            Console.WriteLine(myName);
            
            // Exercise 14
            string myMonth;
            Console.WriteLine("What Month were you born in");
            myMonth = Console.ReadLine();

            // Exercise 15
            int myNumber;
            Console.WriteLine("Please enter a number");
            myNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{myNumber} X 1 = {myNumber * 1}");
            Console.WriteLine($"{myNumber} X 2 = {myNumber * 2}");
            Console.WriteLine($"{myNumber} X 3 = {myNumber * 3}");
            Console.WriteLine($"{myNumber} X 4 = {myNumber * 4}");
            Console.WriteLine($"{myNumber} X 5 = {myNumber * 5}");
            Console.WriteLine($"{myNumber} X 6 = {myNumber * 6}");
            Console.WriteLine($"{myNumber} X 7 = {myNumber * 7}");
            Console.WriteLine($"{myNumber} X 8 = {myNumber * 8}");
            Console.WriteLine($"{myNumber} X 9 = {myNumber * 9}");
            Console.WriteLine($"{myNumber} X 10 = {myNumber * 10}");
            Console.WriteLine($"{myNumber} X 11 = {myNumber * 11}");
            Console.WriteLine($"{myNumber} X 12 = {myNumber * 12}");

            // Exercise 16
            Console.WriteLine("*********************************");
            Console.WriteLine("****    Welcome to my app    ****");
            Console.WriteLine("*********************************");
            Console.WriteLine("");
            Console.WriteLine("*********************************");
            Console.WriteLine("What is your name?");
            myName = Console.ReadLine();
            Console.WriteLine($"Your name is: {myName}");

            // Exercise 17
            string myCake;
            Console.WriteLine("Do you like cake?");
            Console.WriteLine("Yes or No");
            myCake = Console.ReadLine();
            if(myCake == "Yes" || myCake == "yes")
            {
                Console.WriteLine("I like cake too");
            }

            else
            {
                Console.WriteLine("How dare you not like cake");
            }

            // Exercise 18
            // A
            var aNum1 = 1;
            var aNum2 = 2;
            var aNum3 = 3;
            var aNum4 = 4;
            var aNum5 = 5;

            Console.WriteLine($"The Value of aNum1 = {aNum1} ");
            Console.WriteLine($"The Value of aNum2 = {aNum2} ");
            Console.WriteLine($"The Value of aNum3 = {aNum3} ");
            Console.WriteLine($"The Value of aNum4 = {aNum4} ");
            Console.WriteLine($"The Value of aNum5 = {aNum5} ");

            // B
            var bNum1 = 1;
            var bNum2 = 2;
            var bNum3 = 3;
            var bNum4 = 4;
            var bNum5 = 5;
            var bNum6 = 6;

            Console.WriteLine($"bNum1 + bNum2 = {bNum1 + bNum2}");
            Console.WriteLine($"bNum3 + bNum4 = {bNum3 + bNum4}");
            Console.WriteLine($"bNum5 + bNum6 = {bNum5 + bNum6}");

            // C
            int cNum1 = 1;
            int cNum2 = 2;
            int cNum3 = 3;
            int cNum4 = 4;
            
            Console.WriteLine(cNum1 += (cNum2 + cNum3 + cNum4));

            // D
            var thought = "I am thinking";
            //Console.WriteLine(thought * 3);

            // Exercise 19
            Console.WriteLine((6+7) * (3-2));
            Console.WriteLine((6*7) + (3*2));
            Console.WriteLine((6*7) + 3*2);
            Console.WriteLine((3*2) + 6*7);
            Console.WriteLine((3*2) + 7*6/2);
            Console.WriteLine(6+7 * 3-2);
            Console.WriteLine(3*2 + (3*2));
            Console.WriteLine((6*7) * 7+6);
            Console.WriteLine((2*2) + 2*2);
            Console.WriteLine(3*3 + (3*3));
            Console.WriteLine((62+7) * 3+2);
            Console.WriteLine((3*2) + 32*2);
            Console.WriteLine((6*(7+7))/6);
            Console.WriteLine(((2+2) + (2*2)));
            Console.WriteLine(4*4 + (32*32));

            // Exercise 20
            // A
            int myNum1;
            Console.WriteLine("Please enter a number");
            myNum1 = Convert.ToInt32(Console.ReadLine());
            int myNum2;
            Console.WriteLine("Please enter a second number");
            myNum2 = Convert.ToInt32(Console.ReadLine());

            if (myNum1 > myNum2)
            {
                Console.WriteLine($"Your first number ({myNum1}) is larger than your second number ({myNum2})");
            }
            else if (myNum1 < myNum2)
            {
                Console.WriteLine($"Your first number ({myNum1}) is smaller than your second number ({myNum2})");
            }
            else if (myNum1 == myNum2)
            {
                Console.WriteLine("Your two numbers are the same");
            }

            // B
            string passWord;
            Console.WriteLine("Please Enter a password");
            passWord = Console.ReadLine();

            if (passWord.Length < 8)
            {
                Console.WriteLine("Your password was invalid, it was too short");
            }
            else
            {
                Console.WriteLine("Congratulations on your fancy new password");
            }

            // C

            string passWordTemp;
            Console.WriteLine("Please change your password");
            passWordTemp = Console.ReadLine();
            if (passWord == passWordTemp)
            {
                Console.WriteLine("Sorry this password is the same as your previous one");
            }
            else 
            {
                passWord.Replace(passWord, passWordTemp);
                passWordTemp = "";
            }

            
            // Exercise 21
            



            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();

        }
    }
}
